This code was used to submit `dnabushmeat` jobs on the MBB cluster.

If you want to use it, please modify it to fit your needs.

`SGE` was used on the cluster, and jobs were submitted to `mbb.q` queue (`QUEUE=` in these scripts).

Worker nodes should also be set to be submit hosts.

## Software requirements:

  - On master node :

    - SGE
    - zip
    - gnu awk (gawk)

  - On worker nodes :

    - environment module with [module files](https://gitlab.mbb.univ-montp2.fr/mbb/modulefiles) (may be bypassed),
    - python 2.7 (it should also work with python3)
    - gnu awk (gawk)
    - R >=3.2 (with ape)
    - blast 2.8.2
    - clustal 2.1
    - muscle 3.8.31

[`mbbqsub`](https://gitlab.mbb.univ-montp2.fr/mbb/share_wrappers/-/blob/master/mbbqsub) is just a basic wrapper to `qsub` adding some basic metadata for MBB daemon.

*DNAbushmeat DB* (using makeblastdb) is available in `dnabush` directory.
