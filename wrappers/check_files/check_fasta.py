#! /usr/bin/env python

# need python >3.6
#from Bio import SeqIO
import re



#def is_fasta(filename):
#    with open(filename, "r") as handle:
#        fasta = SeqIO.parse(handle, "fasta")
#        return any(fasta)  # False when `fasta` is empty, i.e. wasn't a FASTA file

def replace_gt_in_real_fasta(filename):
        with open(filename, "r") as handle: 
            content = handle.read()
            pattern_seq_line = r"(^&gt;{1})(.+\n?[ATGCYUN]+\n?)"
            new_content = re.sub(pattern_seq_line, r">\2", content, flags=re.M)
        with open(filename, "w") as handle: 
            handle.write(new_content)

