#!/usr/bin/env python

import os

def file_exists(filename):
    if not os.path.isfile(filename):
        print("No such file : %s"%filename)
    else:
        return True