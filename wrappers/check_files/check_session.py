#!/usr/bin/env python

def get_uuid():
    with open(".session_id_user", "r") as handle:
        content = handle.read()
    return str.strip(content)