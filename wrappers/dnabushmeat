#! /usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import datetime
import argparse
#import magic
import shutil
from check_files import check_fasta, check_file, check_session

NUM_THREADS=4
QUEUE="mbb.q"
DB_FILE_PATH="/share/bio/dnabush"
MAX_TARGET_SEQS="30"

if __name__=="__main__":
    # -T do not process tree
    # -m choose evolutionnary model
    # -e evalue
    usage = "dnabushmeat [-1] [-2] [-3] [-4] [-e evalue] [-m model] in_file_path"
    #usage = "dnabushmeat [-1] [-2] [-3] [-4] [-e evalue] [-m model] [-T] in_file_path"
    parser = argparse.ArgumentParser(description="The dnabushmeat "+\
    " wrapper from the HPC ISE-M Team.", prog='dnabushmeat', usage = \
    '%(prog)s [options] \n\n', formatter_class = \
    argparse.RawTextHelpFormatter)

    parser.add_argument("-m","--model", dest="model", choices= \
    ["raw","N","TS","TV","JC69","K80","F81","K81","F84", \
    "BH87","T92","TN93","GG95","logdet","paralin","indel"], \
    default="K80")
    parser.add_argument("-e", dest="evalue", default="10")
    parser.add_argument("-1", dest="gene1",action="store_true")
    parser.add_argument("-2", dest="gene2",action="store_true")
    parser.add_argument("-3", dest="gene3",action="store_true")
    parser.add_argument("-4", dest="gene4",action="store_true")
    parser.add_argument("--query", dest="query_file_path", required=True)

    parser.add_argument('--version', action='version', \
    version='%(prog)s 0.1')

    results = parser.parse_args()

    if len(sys.argv) < 1:
        parser.print_help()
        sys.exit("\nYou did not specify enough argument !!\n\n")

    gene_filenames = ["%s/Bushmeat_12Sweb.fas_db"%(DB_FILE_PATH) ,
             "%s/Bushmeat_16Sweb.fas_db"%(DB_FILE_PATH),
             "%s/Bushmeat_COIweb.fas_db"%(DB_FILE_PATH),
             "%s/Bushmeat_cytbweb.fas_db"%(DB_FILE_PATH)]

    evalue = "1e-"+results.evalue
    model = results.model
    query_file_path = results.query_file_path
    #filetype_query_file_path = magic.from_file(query_file_path)

    """
    Removing /var/www from path if symbolic link and copy
    the file before modifying/checking this file
    """
    #if 'symbolic' in filetype_query_file_path:
    try:
        shortpath = os.readlink(query_file_path)[8:]
        os.unlink(query_file_path)
        #shortpath=filetype_query_file_path[25:]
        mbbrealpath="/home/mbb" + shortpath
        shutil.copyfile(mbbrealpath, query_file_path)
    except:
        print("# Ok. Not a symlink")
    if check_file.file_exists(".session_id_user"):
        uuid = check_session.get_uuid()
        # if user is guest, ">" are replaced by "&gt;" in input files
        if uuid == "0":
            check_fasta.replace_gt_in_real_fasta(query_file_path)
    #check_fasta.replace_gt_in_real_fasta(query_file_path)
    # we add an identificator at the file beginning in case there isn't
    add_id = 'if [ `head -n 1 %s | grep "^>" | wc -l` -eq 0 ]; then echo ">my_query" > identificator ; cat %s >> identificator; mv identificator %s ; fi' % (query_file_path,query_file_path,query_file_path)
    os.system(add_id)
    # we numerotate each query to avoid identical names
    awk_numerotation = "gawk 'BEGIN {i=1} {if ($1 ~ /^>/) {gsub(/^>/,\">\" i \"_\"); gsub(/\|/,\"-\"); gsub(/:/,\"-\"); gsub(/\(/,\"-\"); gsub(/\)/,\"-\"); i++; print } else print}' %s > numeroted ; mv numeroted %s"%(query_file_path,query_file_path)
    os.system(awk_numerotation)
    """
    query_file_path = sys.argv[-1]
    geneBools = [("-1" in sys.argv),
            ("-2" in sys.argv),
            ("-3" in sys.argv),
            ("-4" in sys.argv)]
    """

    #dynamic assign would be better
    geneBool1 = results.gene1
    geneBool2 = results.gene2
    geneBool3 = results.gene3
    geneBool4 = results.gene4

    geneBool = ""
    nbjobs = 0
    array_db = ""
    db_names = ""
    for i in range(4):
        exec("geneBool = geneBool%s"%(i+1))
        if geneBool:
            nbjobs+=1
            array_db += " %s"%(gene_filenames[i])
            db_names += " %s"%(gene_filenames[i].split("meat_")[-1].split("web.")[0])
    array_db = array_db.strip()
    db_names = db_names.strip()

    farray = open('array_dnabush.qsub','w')
    datenow = datetime.datetime.now().strftime('%Y%m%d')

    txt = """#!/bin/bash
#$ -S /bin/bash
#$ -cwd
#$ -sync y
#$ -N dnabushmeat_%s
#$ -q %s
#$ -t 1-%s

DB_TAB=(%s)
TMO=`let B=$SGE_TASK_ID-1;echo $B`
DB_FILE=${DB_TAB[$TMO]}
DB_FILE_FASTA=`echo $DB_FILE | sed 's/...$//'`
BLAST_RESULT=blast_result_`basename $DB_FILE_FASTA`.txt
ALIGN_RESULT=`basename $DB_FILE_FASTA`.aln
DB_NAMES=(%s)
RTREE_NAME=Tree_all_sequences_${DB_NAMES[$TMO]}

module load R-3.2.0
module load blast-2.8.1

blastn -query %s -db "$DB_FILE" -out $BLAST_RESULT -evalue %s -dust no -task dc-megablast -num_threads %s -max_target_seqs %s -outfmt 6 -perc_identity 50.00

if [[ -s $BLAST_RESULT ]]; then
    sort -nr -k4 -k3 $BLAST_RESULT | head -30 > $BLAST_RESULT.tmp
    sort -u -k1,1 $BLAST_RESULT.tmp > $BLAST_RESULT.tmp.2
    mv $BLAST_RESULT.tmp.2 $BLAST_RESULT.tmp
    GENE_DB_TMP=$SGE_TASK_ID.query.fas

    #cp $DB_FILE_FASTA ./$GENE_DB_TMP
    cp $DB_FILE_FASTA.profile ./
    LOCAL_DB_FILE_FASTA_PROFILE=./`basename $DB_FILE_FASTA`.profile

    l_seq_name=`gawk 'BEGIN{ORS="|"} {print $1}' $BLAST_RESULT.tmp`
    l_seq_name=`echo $l_seq_name|sed 's/[()]/_/g'`

    # remove last character (it was a pipe)
    l_seq_name="${l_seq_name%%?}"

    gawk  "/>/ {e=0} /$l_seq_name/ {e=1}{if(e==1){print}}" %s >> $GENE_DB_TMP

    clustalw -PROFILE1=$LOCAL_DB_FILE_FASTA_PROFILE -PROFILE2=$GENE_DB_TMP -OUTFILE=$ALIGN_RESULT -OUTPUTTREE=nj

    l_seq_name=`echo $l_seq_name|sed 's/|/,/'`
    echo $l_seq_name >> seq_names
    #/usr/bin/R --slave --vanilla --quiet  << EOF
    cat << EOF > r_gen_$SGE_TASK_ID
library(ape)

file = \"$ALIGN_RESULT\"
modele = "%s" #"K80"
query = "$l_seq_name" #"paorurus_africanusCAM015_Cam,atrurus_africanusCAMP001_Cam"
prefix= "$RTREE_NAME"
nbBoots = 100


#read alignment
align = read.dna(file, format = "clustal")

f <- function(x, model = modele)
{
    arbre = njs(dist.dna(x, model = modele))
    makeLabel(arbre , space="" )
}

#draw a Neighbor Joinin tree for the given alignment
treeNJ = f(align, modele)

#draw a Neighbor Joinin tree for the given alignment
treeNJ <- makeLabel(treeNJ, space="") # get rid of spaces in tip names.
#bootstrap
valboots = treeNJ\$node.label
try( {
    valboots = boot.phylo(treeNJ, align, f, B = nbBoots,  quiet = TRUE);
    valboots = round(valboots * 100 / nbBoots);
} , silent=TRUE)
#ajouter les valeurs de bootstrap à l'arbre
treeNJ\$node.label = valboots

#save the tree
write.tree(treeNJ, file = paste(prefix,".tre", sep="")) 

EOF
R CMD BATCH --slave --vanilla --quiet r_gen_$SGE_TASK_ID

    # production of individual trees by query
    #/usr/bin/R --slave --vanilla --quiet  << EOF
    cat << EOF > r_ind_$SGE_TASK_ID
Draw_NJTree <- function(query, blast, querySequences, TargetSequences, prefix, modele, nbBoots )
{
    #extraire la liste des hits de la query
    filtredblast = blast[blast\$V1 == query,]

    #les écrire dans un fichier fasta
    fastafile = paste(query,prefix,"_temp.fasta",sep="")
    cat(file= fastafile, append=FALSE)

    for (i in 1: length(filtredblast\$V1) )
    {
        hit = filtredblast\$V2[i]
        cat(hit,"\n")
        sequ = paste(TargetSequences[[hit]], sep="", collapse="")
        cat (file=fastafile,">",hit,"\n", sequ,"\n", sep="",append = TRUE)
    }

    #ajouter le contenu la sequence de la query au fichier des hits
    seqQuery = paste(querySequences[[query]], sep="", collapse="")
    cat (file=fastafile,">",query,"\n", seqQuery,"\n", sep="",append = TRUE)

    alignfile = paste(fastafile,".aln", sep="")
    system(paste("/share/apps/bin/muscle3.8.31_i86linux64 -in ", fastafile, " -out ", alignfile, sep=""))

    #read the alignment
    align = read.dna(alignfile, format = "fasta") 

    #calculate a distance following a provided model then draw an NJ tree
    f <- function(x, model = modele)
    {
        arbre = njs(dist.dna(x, model = modele))
        makeLabel(arbre , space="" )
    }

    #draw a Neighbor Joinin tree for the given alignment
    treeNJ = f(align, modele)
    treeNJ <- makeLabel(treeNJ, space="") # get rid of spaces in tip names.
    #bootstrap
    valboots = treeNJ\$node.label
    try({
        valboots = boot.phylo(treeNJ, align, f, B = nbBoots,  quiet = TRUE);
        valboots = round(valboots * 100 / nbBoots);
    }, silent=T)
    #ajouter les valeurs de bootstrap à l'arbre
    treeNJ\$node.label = valboots

    #save the tree
    write.tree(treeNJ, file = paste(query,prefix,".indtre", sep=""))

    file.remove(fastafile)
}

library(ape)

querySequencesFile     = "%s" #args[1]
TargetsSequenencesFile = "$DB_FILE_FASTA" #args[2]
blastresult = "$BLAST_RESULT"
model    = "K80" #args[5]
nbBoots  = 500 # as.integer(args[6]) #nombre de bootstraps 100

prefix = paste("__",basename(TargetsSequenencesFile), sep="")

#lire le résultat du blast (celui-ci peut contenir plusieurs query)
blast = read.table(blastresult, header=F, sep ="\t", stringsAsFactors = FALSE)

#extraire la liste des query qui ont un hit
queries = unique(blast\$V1)

#lire toutes les séquences Target
TargetSequences = read.dna(TargetsSequenencesFile, format = "fasta", as.character = TRUE, as.matrix=FALSE)

#lire les sequences query
querySequences = read.dna(querySequencesFile, format = "fasta", as.character = TRUE, as.matrix=FALSE)

for (i in 1:length(queries) )
{
   Draw_NJTree(queries[i], blast, querySequences, TargetSequences, prefix, model, nbBoots)
} 
EOF

R CMD BATCH --slave --vanilla --quiet r_ind_$SGE_TASK_ID

# add tab header for blat result
echo "Query	Subject	%%id	alignment length	mistmatches	gap openings	q.start	q.end	s.start	s.end	e-value	bit score" > aa
cat $BLAST_RESULT >> aa
mv aa ${BLAST_RESULT}_with_header.csv

fi

# TODO uncomment rm -f $GENE_DB_TMP $BLAST_RESULT.tmp
# DOES NOT WORK ON THE NODES
#rename 's/\.fas\.outfmt6/.outfmt6.fas/g' *.fas.outfmt6
#for fic in *.fas.outfmt6; do mv "${fic}" "${fic/.fas.outfmt6/.outfmt6.fas}"; done
#mv $BLAST_RESULT "${BLAST_RESULT/.fas.outfmt6/.outfmt6.fas}"

"""%(datenow, QUEUE, nbjobs, array_db, db_names, query_file_path, evalue, NUM_THREADS, MAX_TARGET_SEQS, query_file_path, model, query_file_path)


    farray.write(txt)
    farray.close()



    cmd = "/share/apps/wrappers/mbbqsub `/opt/gridengine/bin/linux-x64/qsub array_dnabush.qsub -V `"
    os.system(cmd)

    # zip results if they exist
    cmd = 'if [ -f *.tre ] || [ -f *.indtre ] || [ -f blast*.fas.txt_with_header.csv ] || [ -f *.aln ]; then mkdir results_%s ; cp *.tre *.indtre blast*.fas.txt_with_header.csv *.aln results_%s; zip -r results.zip results_%s ; rm -rf results_%s; fi'%(datenow,datenow,datenow,datenow)
    os.system(cmd)
